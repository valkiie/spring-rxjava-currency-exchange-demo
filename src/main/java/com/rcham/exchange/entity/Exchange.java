package com.rcham.exchange.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "exchange")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Exchange {
    @Id
    @Column(name="currency")
    private String currency;

    @Column(name="value")
    private Double value;
}
