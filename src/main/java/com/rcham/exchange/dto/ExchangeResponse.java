package com.rcham.exchange.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeResponse {
    private Double amount;
    private Double exchangeAmount;
    private String originCurrency;
    private String targetCurrency;
    private Double exchangeRate;
}
