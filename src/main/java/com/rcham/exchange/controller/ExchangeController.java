package com.rcham.exchange.controller;

import com.rcham.exchange.dto.ExchangeResponse;
import com.rcham.exchange.entity.Exchange;
import com.rcham.exchange.service.ExchangeService;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value="/exchange")
public class ExchangeController {

    @Autowired
    private ExchangeService exService;

    @PostMapping
    public Single<ResponseEntity> save(@RequestBody Exchange exchange) {
        return exService.save(exchange)
                .subscribeOn(Schedulers.io())
                .map(s -> ResponseEntity
                            .created(URI.create("/exchange/"+s))
                            .body("Success"));
    }
    @PostMapping(value="/update")
    public Single<ResponseEntity> update(@RequestBody Exchange exchange) {
        return exService.update(exchange)
                .subscribeOn(Schedulers.io())
                .toSingle(()->ResponseEntity.ok("Updated"));
    }
    @GetMapping
    public Single<ResponseEntity<List<Exchange>>> get() {
        return exService.getAll()
                .subscribeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }

    @GetMapping(value="/{originCurrency}/{targetCurrency}/{amount}")
    public Single<ResponseEntity<ExchangeResponse>> convert(@PathVariable("originCurrency") String originCurrency,
                                                            @PathVariable("targetCurrency") String targetCurrency,
                                                            @PathVariable("amount") Double amount){
        return exService.convert(originCurrency, targetCurrency, amount)
                .subscribeOn(Schedulers.io())
                .map(ResponseEntity::ok);
    }
}
