package com.rcham.exchange.service;

import com.rcham.exchange.dto.ExchangeResponse;
import com.rcham.exchange.entity.Exchange;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;

import java.util.List;

public interface ExchangeService {
    Single<String> save(Exchange exchange);
    Completable update(Exchange exchange);
    Single<List<Exchange>> getAll();
    Single<Exchange> getByCurrency(String currency);
    Single<ExchangeResponse> convert(String origin, String target, Double amount);
}
