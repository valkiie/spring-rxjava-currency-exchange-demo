package com.rcham.exchange.service;

import com.rcham.exchange.dto.ExchangeResponse;
import com.rcham.exchange.entity.Exchange;
import com.rcham.exchange.repository.ExchangeRepository;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class ExchangeServiceImpl implements ExchangeService{

    @Autowired
    private ExchangeRepository exchangeRepo;

    @Override
    public Single<String> save(Exchange exchange) {
        return Single.create(singleSub -> {
           Optional<Exchange> optionalExchange = exchangeRepo.findById(exchange.getCurrency());
           if(optionalExchange.isPresent())
               singleSub.onError(new EntityNotFoundException());
           else {
               String addedExchange = exchangeRepo.save(exchange).getCurrency();
               singleSub.onSuccess(addedExchange);
           }
        });
    }

    @Override
    public Completable update(Exchange exchange) {
        return Completable.create(completableSub -> {
            Optional<Exchange> opExchange = exchangeRepo.findById(exchange.getCurrency());
            if(!opExchange.isPresent())
                completableSub.onError(new EntityNotFoundException());
            else {
                exchangeRepo.save(exchange);
                completableSub.onComplete();
            }
        });
    }

    @Override
    public Single<List<Exchange>> getAll() {
        return Single.create(singleSub -> {
            List<Exchange> exchanges = exchangeRepo.findAll();
            singleSub.onSuccess(exchanges);
        });
    }

    @Override
    public Single<Exchange> getByCurrency(String currency) {
        return Single.create(singleSub -> {
           Optional<Exchange> opExchange = exchangeRepo.findById(currency);
           if(opExchange.isPresent()){
               singleSub.onSuccess(opExchange.get());
           }else
               singleSub.onError(new EntityNotFoundException());
        });
    }

    @Override
    public Single<ExchangeResponse> convert(String origin, String target, Double amount) {
        return Single.create(singleSub->{
            Optional<Exchange> originEx = exchangeRepo.findById(origin);
            if(!originEx.isPresent())
                singleSub.onError(new EntityNotFoundException());
            Optional<Exchange> targetEx = exchangeRepo.findById(target);
            if(!targetEx.isPresent())
                singleSub.onError(new EntityNotFoundException());
            ExchangeResponse result = new ExchangeResponse();
            //Origin Currency to USD
            result.setAmount(amount);
            result.setOriginCurrency(origin);
            result.setTargetCurrency(target);
            result.setExchangeRate(targetEx.get().getValue()/originEx.get().getValue());
            result.setExchangeAmount(amount*targetEx.get().getValue()/originEx.get().getValue());
            singleSub.onSuccess(result);
        });
    }
}
