FROM adoptopenjdk/openjdk11-openj9:centos

COPY ./target/exchange-0.0.1-SNAPSHOT.jar /usr/app/

WORKDIR /usr/app

RUN sh -c 'touch exchange-0.0.1-SNAPSHOT.jar'

ENTRYPOINT ["java","-jar","exchange-0.0.1-SNAPSHOT.jar"]